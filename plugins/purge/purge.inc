<?php

/*
 * includes/purge.inc
 *
 * Most of this code has been copy/pasted from the expire module: http://drupal.org/project/expire
 * We've renamed it to purge, but that has nothing to do with the contrib module purge.
 * (Yeah, let's keep things simple :)
 *
 * _cache_derivative invokes our own steroids plugins to purge url's.
 * For now there is only the varnish module that has the purge() function, but support for squid and other reverse proxies is easily added.
 */

define('STEROIDS_PURGE_FRONTPAGE', TRUE);
define('STEROIDS_PURGE_NODE_TERMS', TRUE);
define('STEROIDS_PURGE_MENU_ITEMS', TRUE);
define('STEROIDS_PURGE_CCK_REFERENCES', TRUE);
define('STEROIDS_INCLUDE_BASE_URL', FALSE);

// @TODO: remove this code
function expire_print_r($data) {
  return str_replace('    ', '&nbsp;&nbsp;&nbsp;&nbsp;', nl2br(htmlentities(print_r($data, TRUE))));
}

class steroids_purge extends steroids_class {

  private $purge_classes = array();

  /**
   *
   * Constructor
   */
  public function __construct() {

    // a list of plugins that can purge()
    $this->purge_classes = array('steroids_varnish');
  }

  /**
   * Settings form
   * @param unknown_type $form_state
   */
  public function settings_form(&$form_state) {
    $form['steroids_purge'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('What to purge'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['steroids_purge']['steroids_purge_frontpage'] = array(
      '#type'        => 'checkbox',
      '#title'       => t('Purge front page'),
      '#default_value' => variable_get('expire_flush_front', STEROIDS_PURGE_FRONTPAGE),
      '#description'   => t('Always purge the frontpage on every expire call.'),
    );

    $form['steroids_purge']['steroids_purge_node_terms'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Purge node term pages'),
      '#default_value' => variable_get('steroids_purge_node_terms', STEROIDS_PURGE_NODE_TERMS),
      '#description'   => t('When purging a node: expire taxonomy pages for its terms.'),
    );

    $form['steroids_purge']['steroids_purge_menu_items'] = array(
      '#type'          => 'radios',
      '#title'         => t('Purge menus'),
      '#options'       => array(0 => t('No'), STEROIDS_PURGE_MENU_ITEMS => t('Family'), 2 => t('Entire menu')),
      '#default_value' => variable_get('steroids_purge_menu_items', STEROIDS_PURGE_MENU_ITEMS),
      '#description'   => t('When purging a node: expire related menu items or entire menu'),
    );
    $form['steroids_purge']['steroids_purge_cck_references'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Purge CCK node references'),
      '#default_value' => variable_get('steroids_purge_cck_references', STEROIDS_PURGE_CCK_REFERENCES),
      '#description'   => t('When purging a node: expire its node references and nodes containing it in their own ndoe references.'),
    );
    $form['steroids_purge']['steroids_include_base_url'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Include base URL in expires'),
      '#default_value' => variable_get('steroids_include_base_url', STEROIDS_INCLUDE_BASE_URL),
      '#description'   => t('Include the base URL in purge requests. Compatible with Domain Access'),
    );

    return $form;

  }

  /**
   * Settings form validation
   */
  public function settings_form_validate(&$form, &$form_state) {
  }

  /**
   * Delegation of hook_nodeapi().
   */
  public function hook_nodeapi(&$node, $op, $a3, $a4) {

    switch ($op) {
      case 'insert':
      case 'update':
    	case 'delete':
    	case 'delete revision':

        $paths = array();

        // @TODO: hook to check if there are other modules that have paths for us that need to be purged


        // call the purge action for all our purg-capable plugins
        $this->_purge_node($node, $paths);
        break;
    }
  }

  // @TODO
  private function _purge_comment() {

  }

  /**
   *
   * Purge old the url's related to a node.
   *
   * @param object $node The node we will build paths for.
   * @param array $paths An array of extra paths that need to be purged also.
   */
  private function _purge_node(&$node, $paths = array()) {

    // Check node object
    if (empty($node->nid)) {
      return FALSE;
    }

    // Expire this node
    $paths['node'] = 'node/' . $node->nid;

    // Do we need to expire the frontpage?
    if (variable_get('steroids_purge_frontpage', STEROIDS_PURGE_FRONTPAGE)) {
      $paths['front'] = '<front>';
    }

    // Get taxonomy terms and flush
    if (module_exists('taxonomy') && variable_get('steroids_purge_node_terms', STEROIDS_PURGE_NODE_TERMS)) {

      // Get old terms from DB
      $tids = $this->_taxonomy_node_get_tids($node->nid);

      // Get old terms from static variable
      $terms = taxonomy_node_get_terms($node);
      if (!empty($terms)) {
        foreach ($terms as $term) {
          $tids[$term->tid] = $term->tid;
        }
      }

      // Get new terms from node object
      if (!empty($node->taxonomy)) {
        foreach ($node->taxonomy as $vocab) {
          if (is_array($vocab)) {
            foreach ($vocab as $term) {
              $tids[$term] = $term;
            }
          }
        }
      }

      $filenames = array();
      foreach ($tids as $tid) {
        if (is_numeric($tid)) {
          $term = taxonomy_get_term($tid);
          $paths['term' . $tid] = taxonomy_term_path($term);
        }
      }
    }

    // Get menu and flush related items in the menu.
    if (variable_get('steroids_purge_menu_items', STEROIDS_PURGE_MENU_ITEMS) != 0) {

      if (!isset($node->menu['menu_name'])) {
        menu_nodeapi($node, 'prepare');
      }

      $menu = menu_tree_all_data($node->menu['menu_name']);
      $tempa = NULL;
      $tempb = NULL;

      if (variable_get('steroids_purge_menu_items', STEROIDS_PURGE_MENU_ITEMS) == 1) {
        $links = $this->_get_menu_structure($menu, FALSE, 'node/' . $node->nid, NULL, $tempa, $tempb);
      }
      elseif (variable_get('steroids_purge_menu_items', STEROIDS_PURGE_MENU_ITEMS) == 2) {
        $links = $this->_get_menu_structure($menu, NULL, NULL, NULL, $tempa, $tempb);
      }

      unset($tempa);
      unset($tempb);
      $paths = array_merge($links, $paths);
    }

    // Get CCK References and flush.
    if (variable_get('steroids_purge_cck_references', STEROIDS_PURGE_CCK_REFERENCES) && module_exists('nodereference')) {
      $nids = array();
      $type = content_types($node->type);
      if ($type) {
        foreach ($type['fields'] as $field) {
          // Add referenced nodes to nids. This will clean up nodereferrer fields
          // when the referencing node is updated.
          if ($field['type'] == 'nodereference') {
            $node_field = isset($node->$field['field_name']) ? $node->$field['field_name'] : array();
            foreach ($node_field as $delta => $item) {
              if (is_numeric($item['nid'])) {
                $paths['reference' . $item['nid']] = 'node/'. $item['nid'];
              }
            }

            // Look for node referers without using nodereferrer
            $info = content_database_info($field);
            $table = $info['table'];
            $column = $info['columns']['nid']['column'];
            $results = db_query("SELECT n.nid
              FROM {%s} nr
              INNER JOIN {node} n USING (vid)
              WHERE nr.%s = %d", $table, $column, $node->nid);
            while ($nid = db_result($results)) {
              if (is_numeric($nid)) {
                $paths['referenceparent' . $nid] = 'node/'. $nid;
              }
            }
          }
        }
      }

      // Get CCK references pointing to this node and flush.
      if (module_exists('nodereferrer')) {
        $nids = nodereferrer_referrers($node->nid);
        foreach ($nids as $nid) {
          if (is_numeric($nid['nid'])) {
            $paths['referrer' . $nid['nid']] = 'node/' . $nid['nid'];
          }
        }
      }
    }

    // Flush array of paths
    if (!empty($paths)) {
      $purged = $this->_cache_derivative($paths, $node);
      watchdog('steroids', 'Node !nid was purged resulting in !purged pages being expired from the cache',  array(
        '!nid' => $node->nid,
        '!purged' => $purged,
      ));
    }
  }

  // @TODO
  private function _purge_nodequeue() {

  }

  // @TODO
  private function _purge_user() {

  }

  /**
   * Return taxonomy terms given a nid.
   *
   * Needed because of a weird bug with CCK & node_load()
   *  http://drupal.org/node/545922
   */
  private function _taxonomy_node_get_tids($nid) {
    $vid = db_result(db_query("SELECT vid FROM {node} WHERE nid = %d", $nid));
    $result = db_query(db_rewrite_sql("SELECT t.tid
                                      	FROM {term_node} r
                                      		INNER JOIN {term_data} t ON r.tid = t.tid
                                      			INNER JOIN {vocabulary} v ON t.vid = v.vid
                                      	WHERE r.vid = %d
                                      	ORDER BY v.weight, t.weight, t.name", 't', 'tid'), $vid);
    $tids = array();
    while ($term = db_result($result)) {
      $tids[] = $term;
    }
    return $tids;
  }

  /**
   * Finds parent, siblings and children of the menu item. UGLY CODE...
   *
   * @param array $menu
   *  Output from menu_tree_all_data()
   * @param bool $found
   *  Signal for when the needle was found in the menu array.
   *  Set TRUE to get entire menu
   * @param string $needle
   *  Name of menu link. Example 'node/21'
   * @param bool $first
   *  Keep track of the first call; this is a recursive function.
   * @param bool &$found_global
   *  Used to signal the parent item was found in one of it's children
   * @param bool &$menu_out
   *  Output array of parent, siblings and children menu links
   */
  private function _get_menu_structure($menu, $found, $needle, $first, &$found_global, &$menu_out) {
    // Set Defaults
    $found = !is_null($found) ? $found : TRUE;
    $needle = !is_null($needle) ? $needle : '';
    $first = !is_null($first) ? $first : TRUE;
    $found_global = FALSE;
    $menu_out = !is_null($menu_out) ? $menu_out : array();

    // Get Siblings
    foreach ($menu as $item) {
      if ($item['link']['hidden'] == 0 && $item['link']['page_callback'] != '' && ($item['link']['link_path'] == $needle || $found)) {
        $menu_out[] = $item['link']['link_path'];
        $found = TRUE;
      }
    }
    // Get Children
    foreach ($menu as $item) {
      if ($item['link']['hidden'] != 0) {
        continue;
      }
      if ($item['link']['page_callback'] != '' && ($item['link']['link_path'] == $needle || $found)) {
        $menu_out[] = $item['link']['link_path'];
        $found = TRUE;
      }
      // Get Grandkids
      if (!empty($item['below'])) {
        $sub_menu = array();
        foreach ($item['below'] as $below) {
          if ($below['link']['hidden'] == 0) {
            $sub_menu[] = $below;
          }
        }
        $this->_get_menu_structure($sub_menu, $needle, $found, FALSE, $found_global, $menu_out);
        $structure[$item['link']['link_path']][] = $sub;
        if ($item['link']['page_callback'] != '' && $found_global) {
          // Get Parent of kid
          $menu_out[] = $item['link']['link_path'];
        }
      }
      else {
        $structure[$item['link']['link_path']] = '';
      }
    }

    // Clean up
    if (isset($structure) && is_array($structure)) {
      $structure = array_unique($structure);
    }
    $found_global = $found;
    if ($first) {
      if (isset($menu_out) && is_array($menu_out)) {
        $menu_out = array_unique($menu_out);
        sort($menu_out);
        return $menu_out;
      }
      else {
        return array();
      }
    }
    else {
      return $structure;
    }
  }

  /**
   * Get all base url's where this node can appear. Domain access support.
   *
   * @param $node
   *   node object
   * @return array
   *   array(0 => array($base_url . '/'))
   */
  private function _get_base_urls(&$node) {
    global $base_url, $base_path;

    // Get list of URL's if using domain access
    $base_urls = array();
    $domains = array();
    if (module_exists('domain') && isset($node->domains)) {
      // Get domains from node/user object
      foreach ($node->domains as $key => $domain_id) {
        if ($key != $domain_id) {
          continue;
        }
        $domains[$domain_id] = $domain_id;
      }
      // Get domains from database
      foreach (expire_get_domains($node) as $domain_id) {
        $domains[$domain_id] = $domain_id;
      }
      // Get aliases and set base url
      foreach ($domains as $domain_id) {
        $domain = domain_lookup($domain_id);
        if ($domain['valid'] == 1) {
          if (isset($domain['path'])) {
            $base_urls[$domain_id][] = $domain['path'];
          }
          if (is_array($domain['aliases'])) {
            foreach ($domain['aliases'] as $alias) {
              if ($alias['redirect'] != 1) {
                $temp_domain = array('scheme' => $domain['scheme'], 'subdomain' => $alias['pattern']);
                $base_urls[$domain_id][] = domain_get_path($temp_domain);
              }
            }
          }
        }
      }
    }
    else {
      $base_urls[0][] = $base_url . '/';
    }
    return $base_urls;
  }

  /**
   * Finds all possible paths/redirects/aliases given the root path.
   *
   * @param $paths
   *   Array of current URLs
   * @param $node
   *   node object
   */
  private function _cache_derivative($paths, &$node = NULL) {
    global $base_path;
    $expire = array();

    if (empty($paths)) {
      return FALSE;
    }
    $site_frontpage = variable_get('site_frontpage', 'node');
    foreach ($paths as $path) {
      // Special front page handling
      if ($path == $site_frontpage || $path == '' || $path == '<front>') {
        $expire[] = '';
        $expire[] = 'rss.xml';
        $expire[] = $site_frontpage;
      }

      // Add given path
      if ($path != '<front>') {
        $expire[] = $path;
      }

      // Path alias
      $path_alias = url($path, array('absolute' => FALSE));
      // Remove the base path
      $expire[] = substr($path_alias, strlen($base_path));

      // Path redirects
      if (module_exists('path_redirect')) {
        $path_redirects = $this->_path_redirect_load(array('redirect' => $path));
        if (isset($path_redirects)) {
          foreach ($path_redirects as $path_redirect) {
            $expire[] = $path_redirect['path'];
          }
        }
      }
    }

    // Expire cached files
    if (empty($expire)) {
      return FALSE;
    }
    $expire = array_unique($expire);
    // Add on the url to these paths
    $urls = array();
    global $base_url;
    if (variable_get('steroids_include_base_url', STEROIDS_INCLUDE_BASE_URL)) {
      foreach ($this->_get_base_urls($node) as $domain_id) {
        foreach ($domain_id as $base) {
          foreach ($expire as $path) {
            $urls[] = $base . $path;
          }
        }
      }
    }
    else {
      $urls = $expire;
    }

    // here we actually call all our registered plugins to purge the paths we built up
    $modules = array();
    $count = 0;
    foreach ((array)$this->purge_classes as $plugin) {
      $object = _steroids_get_plugin($plugin);
      if (method_exists($object, 'purge')) {
        $modules[] = $purger['class'];
        $count += $object->purge($urls);
      }
    }

    watchdog('steroids', 'Input: !paths <br /> Output: !urls <br /> Modules Using hook_expire_cache(): !modules', array(
      '!paths' => expire_print_r($paths),
      '!urls' => expire_print_r($urls),
      '!modules' => expire_print_r($modules),
    ));
    return count($urls);
  }

  /**
   * Retrieve a specific URL redirect from the database.
   * http://drupal.org/node/451790
   *
   * @param $where
   *   Array containing 'redirect' => $path
   */
  private function _path_redirect_load($where = array(), $args = array(), $sort = array()) {
    $redirects = array();
    if (is_numeric($where)) {
      $where = array('rid' => $where);
    }

    foreach ($where as $key => $value) {
      if (is_string($key)) {
        $args[] = $value;
        $where[$key] = $key .' = '. (is_numeric($value) ? '%d' : "'%s'");
      }
    }

    if ($where && $args) {
      $sql = "SELECT * FROM {path_redirect} WHERE ". implode(' AND ', $where);
      if ($sort) {
        $sql .= ' ORDER BY '. implode(' ,', $sort);
      }
      $result = db_query($sql, $args);
      while ($redirect = db_fetch_array($result)) {
        $redirects[] = $redirect;
      }
      return $redirects;
    }
  }

  /**
   *
   * Get the section path for a taxonomy
   * @param mixed $taxonomy
   */
  function _get_section_path($taxonomy) {
    $path = '';
    if (is_array($taxonomy)) {
      $section_tid = FALSE;
      $section_vid = variable_get('sections_vocabulary', '');
      foreach ($taxonomy as $vid => $tid) {
        if ($vid == $section_vid) {
          $section_tid = $tid;
          break;
        }
      }
      if ($section_tid) {
        $nid = db_result(db_query("SELECT nid FROM {sections} WHERE tid = %d", $section_tid));
        $path = db_result(db_query("SELECT dst FROM {url_alias} WHERE src LIKE 'node/%d' ORDER BY pid DESC LIMIT 1", $nid));
      }
    }

    return $path;
  }
}