<?php

class steroids_max_age {

  public function __construct() {
    
  }

  /**
   * Delegation of hook_nodeapi().
   */
  public function hook_nodeapi(&$node, $op, $a3, $a4) {
    switch ($op) {
      case 'load':
        $max_age = db_result(db_query("SELECT cache_max_age FROM {node} WHERE nid = %d", $node->nid));
        $node->cache_max_age = $max_age;
        break;
      case 'insert':
      case 'update':
        db_query("UPDATE {node} SET cache_max_age = '%s' WHERE nid = %d", $node->cache_max_age_length, $node->nid);
        break;
    }

    // Set header on view operation and only on the node page.
    if ($op == 'view' && $a4 == TRUE && $_GET['q'] === 'node/' . $node->nid) {

      if ((empty($node->cache_max_age) || $node->cache_max_age === 'default') && $node->cache_max_age !== '0') {
        $defaults = $this->_get_settings('defaults');
        $node->cache_max_age = $defaults['max_age_node_' . $node->type];
      }

      $this->_max_age_value($node->cache_max_age);
    }
  }

  /**
   * Delegation of hook_exit().
   */
  public function hook_exit() {
    $max_age = $this->_max_age_value();
    drupal_set_header('Cache-Control', 'public, max-age=' . $max_age);
  }

  /**
   * Delegation of hook_form_alter().
   */
  public function hook_form_alter(&$form, $form_state, $form_id) {
    if ($form['#id'] == 'node-form') {
      $form['cache_max_age'] = array(
          '#type' => 'fieldset',
          '#title' => t('Cache settings'),
          '#weight' => 5,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
      );

      if (!empty($form['#node']->cache_max_age)) {
        $default_option = $form['#node']->cache_max_age;
      } 
      else {
        $default_option = 'default';
      }

      $form['cache_max_age']['cache_max_age_length'] = array(
          '#type' => 'select',
          '#title' => t('Cache length'),
          '#options' => $this->_max_age_periods(),
          '#default_value' => $default_option,
      );
    }
  }

  /**
   * Settings form
   */
  public function settings_form(&$form_state) {

    $form['steroids_max_age'] = array(
        '#type' => 'fieldset',
        '#title' => t('Max age'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
    );

    $default_settings = $this->_get_settings('defaults');

    foreach (array_keys(node_get_types('names')) as $type) {
      $form['steroids_max_age']['defaults']['max_age_node_' . $type] = array(
          '#type' => 'select',
          '#title' => t('Page cache maximum age for node type: @type', array('@type' => $type)),
          '#default_value' => $default_settings['max_age_node_' . $type],
          '#options' => $this->_max_age_periods(),
      );
    }

    return $form;
  }

  /**
   * Settings form validation
   */
  public function settings_form_validate(&$form, &$form_state) {
    
  }

  /**
   * Get settings
   */
  private function _get_settings($name = NULL) {
    static $settings;

    if (!isset($settings)) {
      $settings = variable_get('steroids_max_age', array());
    }

    if (!isset($name)) {
      return $settings;
    }

    return $settings[$name];
  }

  private function _max_age_value($value = NULL) {
    static $max_age;

    if (isset($max_age)) {
      return $max_age;
    }

    $max_age = $value;

    return $max_age;
  }

  private function _max_age_periods() {
    $periods = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400, 86400 * 2, 86400 * 5, 86400 * 7, 86400 * 14, 86400 * 21, 86400 * 28), 'format_interval');
    $periods[0] = '<' . t('No cache') . '>';
    $periods['default'] = '<' . t('Default') . '>';

    return $periods;
  }

}
