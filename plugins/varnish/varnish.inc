<?php

/**
 */
class steroids_varnish {

  private $socket; // Open socket pointer
  private $host;
  private $port;

  public function __construct() {

  }

  /**
   * Implements hook_steroids_purge($urls)
   */
  public function hook_steroids_purge($urls) {
    return $this->purge($urls);
  }

  /**
   * Settings form
   */
  public function settings_form(&$form_state) {

    $form['steroids_varnish'] = array(
        '#type' => 'fieldset',
        '#title' => t('Varnish'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
    );

    $form['steroids_varnish']['servers'] = array(
        '#type' => 'textarea',
        '#title' => t('Backend servers'),
        '#default_value' => $this->_get_settings('servers'),
        '#size' => 60,
        '#required' => TRUE,
        '#description' => 'A list of Varnish backend servers, specified as ip:port',
    );

    $form['steroids_varnish']['status'] = array(
        '#value' => $this->_get_status(),
    );

    $form['steroids_varnish']['timeout'] = array(
        '#type' => 'textfield',
        '#title' => t('Timeout'),
        '#default_value' => $this->_get_settings('timeout'),
        '#size' => 60,
        '#required' => TRUE,
    );

    $form['steroids_varnish']['purge'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable purging'),
        '#default_value' => $this->_get_settings('purge'),
    );

    return $form;
  }

  /**
   * Settings form validation
   */
  public function settings_form_validate(&$form, &$form_state) {
    //@TODO Validate ip and port
  }

  /**
   * Get settings
   */
  private function _get_settings($name = NULL) {
    static $settings;

    if (!isset($settings)) {
      $settings = variable_get('steroids_varnish', array());
    }

    if (!isset($name)) {
      return $settings;
    }

    return $settings[$name];
  }

  /**
   * Get servers
   */
  private function _get_servers() {
    static $servers;

    if (isset($servers)) {
      return $servers;
    }

    $servers = array();

    $server_settings = $this->_get_settings('servers');
    $server_settings = preg_split("[\n]", $server_settings);

    foreach ((array) $server_settings as $server) {
      $split = preg_split('[:]', $server);
      $servers[] = array(
          'host' => $split[0],
          'port' => $split[1],
      );
    }

    return $servers;
  }

  /**
   * Get status table
   */
  private function _get_status() {
    // @TODO Fix if server not running not visible in table view
    $status = $this->command('status');

    $header = array(
        t('Host'),
        t('Port'),
        t('Status'),
    );

    $rows = array();

    foreach ((array) $status as $state) {
      $rows[] = array(
          $state['host'],
          $state['port'],
          ($state['code']) ? t('Running') : t('Not running or no response'),
      );
    }

    return theme('table', $header, $rows, array(), t('Server status'));
  }

  /**
   * Perform command
   */
  public function command($cmd, $ok = 200) {
    $servers = $this->_get_servers();
    $response = array();

    if (count($servers)) {
      foreach ($servers as $server) {
        $this->host = $server['host'];
        $this->port = $server['port'];

        if ($this->_connect()) {
          $this->_write($cmd);
          $this->_write("\n");

          $response[] = array(
              'response' => $this->_read($code),
              'code' => $code,
              'host' => $server['host'],
              'port' => $server['port'],
              'cmd' => $cmd,
          );

          if ($code !== $ok) {
            $this->_set_error('Command error');
          }

          fclose($this->socket);
        }
      }
    }
    return $response;
  }

  private function _connect() {

    if (!extension_loaded('sockets')) {
      return FALSE;
    }

    $timeout = $this->_get_settings('timeout');

    if (empty($this->host)) {
      return FALSE;
    }

    $this->socket = fsockopen($this->host, $this->port, $errno, $errstr, $timeout);

    // Failed connection
    if (!is_resource($this->socket)) {
      $this->_set_error($errstr);
      return FALSE;
    }

    stream_set_blocking($this->socket, TRUE);
    stream_set_timeout($this->socket, $timeout);

    // @TODO varnish auth required connections

    $response = $this->_read($code);

    if ($code !== 200) {
      $this->_set_error('Bad response from varnishadm');
      return FALSE;
    }

    return TRUE;
  }

  public function purge($urls) {
    $purge_list = implode('$|^/', $urls);
    $this->command('purge req.url ~ "^' . $purge_list . '$"');
  }

  private function _set_error($errstr) {
    watchdog('steroids', 'Varnish: Error on socket @server:@port: @error', array(
        '@host' => $this->host,
        '@port' => $this->port,
        '@error' => $errstr), WATCHDOG_ERROR
    );
  }

  private function _read(&$code) {
    $code = 0;
    // get bytes until we have either a response code and message length or an end of file
    // code should be on first line, so we should get it in one chunk
    while (!feof($this->socket)) {
      $response = fgets($this->socket, 1024);
      if (!$response) {
        $meta = stream_get_meta_data($this->socket);

        if ($meta['timed_out']) {
          throw new Exception(sprintf('Timed out reading from socket %s:%s', $this->host, $this->port));
        }
      }
      if (preg_match('/^(\d{3}) (\d+)/', $response, $r)) {
        $code = (int) $r[1];
        $len = (int) $r[2];
        break;
      }
    }

    if (!isset($code)) {
      throw new Exception('Failed to get numeric code in response');
    }

    $response = '';
    while (!feof($this->socket) && strlen($response) < $len) {
      $response .= fgets($this->socket, 1024);
    }
    return $response;
  }

  private function _write($data) {
    $bytes = fputs($this->socket, $data);
    if ($bytes !== strlen($data)) {
      $this->_set_error('Failed to write to varnishadm');
    }
    return TRUE;
  }

}
