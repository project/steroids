<?php

/**
 * Theming function for the esi settings table
 */
function theme_steroids_esi_table($form) {
  $header = array(('Name'), t('Status'), t('Cache duration (TTL)'), t('Custom ESI path'));
  $rows = array();

  $keys = element_children($form['blocks']);
  array_pop($keys);

  foreach ($keys as $key) {
    $row = array();
    $row[] = drupal_render($form['blocks'][$key]['name']);
    $row[] = drupal_render($form['blocks'][$key]['enabled']);
    $row[] = drupal_render($form['blocks'][$key]['ttl']);
    $row[] = drupal_render($form['blocks'][$key]['src']);
    //$row[] = l(t('Info'));
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * Create the ESI-tag for a particular block.
 */
function theme_steroids_esi_block($block) {
  global $theme_key, $base_url;

  $object = _steroids_get_plugin($plugin);

  if (!empty($block_cache_settings['src'])) {
    $src = $block_cache_settings['src'];
  } 
  else {
    $bid = $theme_key .':'. $block->region .':'. $block->module .':'. $block->delta;
    $src = $base_url . '/steroids/callback/steroids_esi/block/'. $bid;
  }

  $output  = '';
  $output .= '<esi:try>';
  $output .= '<esi:attempt>';
  $output .= '<esi:include src="'. $src .'" onerror="continue"/>';
  $output .= '</esi:attempt>';
  $output .= '<esi:remove>';
  $output .= t('Block not available');
  $output .= '</esi:remove>';
  $output .= '<esi:try>';

  return $output;
}
