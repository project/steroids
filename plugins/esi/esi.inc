<?php

class steroids_esi {

  /**
   * Delegation of hook_theme().
   */
  public function hook_theme() {
    $items = array();

    $defaults = array(
        'file' => 'esi.theme.inc',
        'path' => drupal_get_path('module', 'steroids') . '/plugins/esi',
    );

    $items['steroids_esi_table'] = array(
        'arguments' => array(
            'form' => NULL
        ),
            ) + $defaults;

    $items['steroids_esi_block'] = array(
        'arguments' => array(
            'block' => array()
        ),
            ) + $defaults;

    return $items;
  }

  /**
   * Delegation of hook_preprocess_block().
   */
  public function hook_preprocess_block(&$vars) {
    $enabled_blocks = $this->_get_enabled_blocks();

    if (in_array($vars['block']->module . '_' . $vars['block']->delta, $enabled_blocks) && $vars['block']->esi_processed != TRUE) {
      $settings = $this->_get_block_settings($vars['block']->module, $vars['block']->delta);

      $vars['block']->src = $settings['src'];
      $vars['block']->ttl = $settings['ttl'];

      $vars['block']->content = theme('steroids_esi_block', $vars['block']);
    }
  }
  
  /**
   * Callbacks
   */
  public function callbacks() {
    $action_request = arg(3);
    
    switch ($action_request) {
      case 'block':
          $response = array(
              'type' => 'print',
              'data' => $this->_get_block(arg(4)),
          );
          return $response;
        break;
      default: return FALSE;
    }
  }

  /**
   * Settings form
   */
  public function settings_form(&$form_state) {

    $form['steroids_esi'] = array(
        '#type' => 'fieldset',
        '#title' => t('ESI Blocks'),
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,
        '#tree' => TRUE,
        '#theme' => 'steroids_esi_table',
    );

    $cache_settings = $this->_get_settings('blocks');

    foreach (module_implements('block') as $module) {
      $module_blocks = module_invoke($module, 'block', 'list');
      if ($module_blocks) {
        foreach ($module_blocks as $delta => $info) {
          $name = check_plain($info['info']);
          $bid = "{$module}_{$delta}";
          $form['steroids_esi']['blocks'][$bid] = array();
          $form['steroids_esi']['blocks'][$bid]['name'] = array('#value' => $name);
          $form['steroids_esi']['blocks'][$bid]['enabled'] = array(
              '#type' => 'checkbox',
              '#title' => t('Enabled'),
              '#default_value' => $cache_settings[$bid]['enabled']
          );
          $form['steroids_esi']['blocks'][$bid]['ttl'] = array(
              '#type' => 'select',
              '#options' => $this->_esi_ttl_periods(),
              '#default_value' => $cache_settings[$bid]['ttl'],
          );
          $form['steroids_esi']['blocks'][$bid]['src'] = array(
              '#type' => 'textfield',
              '#default_value' => $cache_settings[$bid]['src'],
          );
        }
      }
    }
    return $form;
  }
  
  /**
   * Get block
   */
  private function _get_block($bid) {
    
    if (!substr_count($bid, ':') == 3) {
      return FALSE;
    }
    list($theme, $region, $module, $delta) = explode(':', $bid);

    init_theme();

    global $theme_key;
    $theme_key = $theme;

    $block = db_fetch_object(db_query("SELECT * FROM {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $module, $delta, $theme));
    $block->context = $region;
    $block->status = 1;
    $block->esi_processed = TRUE;
    $array = module_invoke($block->module, 'block', 'view', $block->delta);
    if (isset($array) && is_array($array)) {
      foreach ($array as $k => $v) {
         $block->$k = $v;
      }
    }
    if (!empty($block->title)) {
      // Check plain here to allow module generated titles to keep any markup.
      $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
    }
    if (!isset($block->subject)) {
      $block->subject = '';
    }
      
    $config = $this->_get_block_settings($module, $delta);
      
    header('Cache-Control: max-age='. $config['ttl']);

    return theme('block', $block);
  }

  /**
   * Get settings
   */
  private function _get_settings($name = NULL) {
    static $settings;

    if (!isset($settings)) {
      $settings = variable_get('steroids_esi', array());
    }

    if (!isset($name)) {
      return $settings;
    }

    return $settings[$name];
  }

  /**
   * Returns an array of all blocks that are enabled for ESI support
   */
  private function _get_enabled_blocks() {
    static $enabled_blocks;

    if (is_array($enabled_blocks)) {
      return $enabled_blocks;
    }

    $cache_settings = $this->_get_settings('blocks');
    $enabled_blocks = array();

    foreach ((array) $cache_settings as $key => $value) {
      if ($value['enabled'] == 1) {
        array_push($enabled_blocks, $key);
      }
    }

    return $enabled_blocks;
  }

  private function _get_block_settings($module, $delta, $config = NULL) {
    $settings = $this->_get_settings('blocks');
    return $settings[$module . '_' . $delta];
  }

  private function _esi_ttl_periods() {
    $periods = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400, 86400 * 2, 86400 * 5, 86400 * 7, 86400 * 14, 86400 * 21, 86400 * 28), 'format_interval');
    return $periods;
  }

}