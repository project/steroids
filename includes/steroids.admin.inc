<?php

/**
 * Form callback for the Steroids settings form.
 */
function steroids_settings(&$form_state) {
  module_load_include('inc', 'steroids', 'includes/steroids.helpers');

  $form = array();

  $form['help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Help'),
  );

  $form['help']['text'] = array(
      '#value' => '<p>' . t('You can configure different options here.') . '</p>',
  );

  // Plugins
  $plugins = array('steroids_varnish', 'steroids_max_age', 'steroids_esi', 'steroids_purge');

  foreach ($plugins as $plugin) {

    $object = _steroids_get_plugin($plugin);

    if (method_exists($object, 'settings_form')) {
      $form = array_merge($form, $object->settings_form($form_state));
    }
  }

  return system_settings_form($form);
}

/**
 * Form callback settings validation.
 */
function steroids_settings_validate(&$form, &$form_state) {
  // Plugins
  $plugins = array('steroids_varnish', 'steroids_max_age', 'steroids_esi', 'steroids_purge');

  foreach ($plugins as $plugin) {
    $object = _steroids_get_plugin($plugin);
    if (method_exists($object, 'settings_form_validate')) {
      $object->settings_form_validate($form, $form_state);
    }
  }
}