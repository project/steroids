<?php

define('STEROIDS_CLASS_VERSION' , 1);

class steroids_class {

  public $version = STEROIDS_CLASS_VERSION;

  public function __construct() {

  }

  /**
   *
   * Utility debug function
   * @param unknown_type $data
   */
  protected function pr($data) {
    if (module_exists('devel')) {
      dpm($data);
    } else {
      print '<pre>';
      print_r($data);
      print '</pre>';
    }
  }
}